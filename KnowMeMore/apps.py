from django.apps import AppConfig


class KnowmemoreConfig(AppConfig):
    name = 'KnowMeMore'
