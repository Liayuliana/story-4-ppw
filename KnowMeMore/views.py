from django.shortcuts import render

# Create your views here.
def KnowMeMore(request):
    return render(request, 'KnowMeMore/KnowMeMore.html')

